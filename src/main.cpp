/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *	this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *	this list of conditions and the following disclaimer in the documentation
 *	and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *	may be used to endorse or promote products derived from this software
 *	without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *	ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
/**
 * @file        main.cpp
 *
 * @brief       Runs the Depth-From-Stereo (DFS) Server, parsing input images
 * 			    from voxl-camera-server and computing DFS. This server is 
 *              usable with CPU or CVP hardware with default behavior being
 *              the latter. Configuration for this module is primarily done
 *              through the configuration file at 
 *              /etc/modalai/voxl-dfs-server. 
 *
 * @author      James Strawson
 * @author      Thomas Patton (thomas.patton@modalai.com) 
 * @date        2023 
 */

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <time.h>

#include <modal_pipe.h>
#include <modalcv.h>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;

#include "config_file.h"


#define PROCESS_NAME "voxl-dfs-server"

// each stereo pair outputs 3 pipes
// Disparity Image
// Scaled disparity image for preview and debug, max disparity scaled to 255
// Point Cloud
#define OUT_PIPES_PER_PAIR				3
#define PIPE_OFFSET_DISPARITY			0
#define PIPE_SUFFIX_DISPARITY			"disparity"
#define PIPE_OFFSET_DISPARITY_SCALED	1
#define PIPE_SUFFIX_DISPARITY_SCALED	"disparity_scaled"
#define PIPE_OFFSET_POINT_CLOUD			2
#define PIPE_SUFFIX_POINT_CLOUD			"pc"

/* w, h*/
static int width[MAX_PAIRS];
static int height[MAX_PAIRS];

/* debug variables */
static int en_debug;
static int en_timing;
static int n_skipped[MAX_PAIRS];
static int is_using_cvp[MAX_PAIRS];   // if a thread is using CVP

/* storage for input/output buffers */
static mcv_undistort_map_t mapL[MAX_PAIRS];
static mcv_undistort_map_t mapR[MAX_PAIRS];
static float baseline_m[MAX_PAIRS];
static mcv_pc_disp_table_t pc_disp_table[MAX_PAIRS];
static uint8_t* imgL_rect[MAX_PAIRS];
static uint8_t* imgR_rect[MAX_PAIRS];
static uint8_t* disparity_raster[MAX_PAIRS];
static uint8_t* disparity_lin[MAX_PAIRS];
static uint8_t* disparity_scaled[MAX_PAIRS];
static uint8_t* occlusion_mask[MAX_PAIRS];
static uint8_t* occlusion_lin[MAX_PAIRS];

/* store handles either for either DFS or Undistort+DFS */
static mcv_stereo_params_t cvp_stereo_params[MAX_PAIRS];
static mcv_cvp_undistort_grid_t* undistort_grid_l[MAX_PAIRS];
static mcv_cvp_undistort_grid_t* undistort_grid_r[MAX_PAIRS];
static mcv_cvp_dfs_handle dfs_h[MAX_PAIRS];
static mcv_cvp_undistort_and_dfs_handle ud_dfs_h[MAX_PAIRS];

/* Configure a timing buffer if desired*/
static std::vector<double> callback_times;
static const size_t max_vec_size = 500;

/**
 * @brief Helper function for computing the mean of image callback times
 * 
 * @return double 
 */
static double calculate_callback_time_mean() {
	double sum = 0.0;
	for (const double time : callback_times) {
		sum += time;
	}
	return sum / callback_times.size();
}

/**
 * @brief Helper function for computing the std of image callback times
 * 
 * @return double 
 */
static double calculate_callback_time_std(double mean) {
	double variance = 0.0;
	for (const double time : callback_times) {
		variance += std::pow(time - mean, 2);
	}
	variance /= callback_times.size();
	return std::sqrt(variance);
}

static int64_t _apps_time_monotonic_ns()
{
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC, &ts)){
		fprintf(stderr,"ERROR calling clock_gettime\n");
		return -1;
	}
	return (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
}


static void _print_usage(void)
{
	printf("\n\
voxl-dfs-server usually runs as a systemd background service. However, for debug\n\
purposes it can be started from the command line manually with any of the following\n\
debug options. When started from the command line, voxl-dfs-server will automatically\n\
stop the background service so you don't have to stop it manually\n\
-c, --config          load the config file only, for use by the config wizard\n\
-d, --debug           run in debug mode which computes everything even when there\n\
                        are no clients to receive the data.\n\
-h, --help            print this help message\n\
-t, --timing          enable timing mode for debug\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"config",				no_argument,	0, 'c'},
		{"debug",				no_argument,	0, 'd'},
		{"help",				no_argument,	0, 'h'},
		{"timing",				no_argument,	0, 't'},
		{0, 0, 0, 0}
	};

	while (1)
	{
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdht", long_options, &option_index);

		// Detect the end of the options.
		if (c == -1){
			break;
		}

		switch (c){
		case 0:
			// for long args without short equivalent that just set a flag nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			config_file_read();
			exit(0);

		case 'd':
			printf("Enabling debug mode\n");
			en_debug = 1;
			break;

		case 'h':
			_print_usage();
			exit(0);

		case 't':
			en_timing = 1;
			break;

		default:
			// Print the usage if there is an incorrect command line option
			_print_usage();
			return -1;
		}
	}

	return false;
}

/** Terminates the app cleanly.
 *  Call this instead of return when it's time to exit to cleans up everything
 *
 *  @param ret The return code to use when exiting
 */
static void _quit(int ret)
{
	// Close all the open pipe connections
	pipe_server_close_all();
	pipe_client_close_all();

	// free memory if enabled 
	for(int i=0;i<MAX_PAIRS;i++){
		if(conf[i].enable) 
		{
			if(imgL_rect[i]){
				free(imgL_rect[i]);
			}
			if(imgR_rect[i]){
				free(imgR_rect[i]);
			}
			if(disparity_raster[i]){
				free(disparity_raster[i]);
			}
			if(disparity_lin[i]){
				free(disparity_lin[i]);
			}
			if(disparity_scaled[i]){
				free(disparity_scaled[i]);
			}

			// tear down using specified handle
			if(conf[i].en_cvp_rectification)
			{
				mcv_cvp_undistort_and_dfs_deinit(ud_dfs_h[i]);
			} else {
				mcv_cvp_dfs_deinit(dfs_h[i]);
			}
		}
	}

	// Remove this process ID file from the filesystem so this app can run again latter
	remove_pid_file(PROCESS_NAME);

	// If we are exiting cleanly then say so
	if(ret == 0){
		printf("Exiting Cleanly!\n");
	}
	// if desired, print out mean and std of times
	if(en_timing)
	{
		double mean = calculate_callback_time_mean();
		double std  = calculate_callback_time_std(mean);
		printf("Mean Callback Time :: (%0.3f +/- %0.3f) ms\n", mean, std);
	}

	// Exit with the return code
	exit(ret);
	return;
}


// each
static int _has_clients(int pair)
{
	if(pair<0 || pair>=MAX_PAIRS){
		fprintf(stderr, "ERROR in %s, pair out of bounds\n", __FUNCTION__);
		return 0;
	}
	if(pipe_server_get_num_clients((pair*OUT_PIPES_PER_PAIR) + 0)>0){
		return 1;
	}
	if(pipe_server_get_num_clients((pair*OUT_PIPES_PER_PAIR) + 1)>0){
		return 1;
	}
	if(pipe_server_get_num_clients((pair*OUT_PIPES_PER_PAIR) + 2)>0){
		return 1;
	}
	return 0;
}


// Callback for whenever we connect to the camera server
static void _camera_connect_cb(int ch, __attribute__((unused)) void* context)
{
	printf("connected to pipe %s\n", conf[ch].input_pipe);
	return;
}

// Callback for whenever we disconnect from the camera server
static void _camera_disconnect_cb(int ch, __attribute__((unused)) void* context)
{
	printf("disconnected from pipe %s\n", conf[ch].input_pipe);
	return;
}

// callback for when a client connects to us
static void _client_connect_handler(int ch, __attribute__((unused))int client_id, char* name, __attribute__((unused)) void* context)
{
	printf("client %s connected to ch %d\n", name, ch);

	// resume connection to the appropriate camera input channel.
	// if we are already resumed/connected then this does nothing.
	pipe_client_resume(ch/OUT_PIPES_PER_PAIR);
	return;
}

// callback for when a client disconnects from us
static void _client_disconnect_handler(int ch, __attribute__((unused))int client_id, char* name, __attribute__((unused)) void* context)
{
	// // if the last client disconnects and no more are left, pause the connection
	// // to the camera pipe to save resources.
	// if(_has_clients(ch/OUT_PIPES_PER_PAIR)==0){
	// 	pipe_client_pause(ch/OUT_PIPES_PER_PAIR);
	// }
	printf("client %s disonnected from ch %d\n", name, ch);

	return;
}

/**
 * @brief Populate the width and height buffers for a given channel
 * 
 * @param ch Channel
 * @param intrinsics_file Intrinsics file to be read
 * @return 0 if successful, -1 otherwise 
 */
static int _get_w_h_from_intrinsics_file(int ch, const char* intrinsics_file)
{	
	int w = -1;
	int h = -1;
	cv::FileNode n;

	// Read intrinsic parameters
	cv::FileStorage fs(intrinsics_file, cv::FileStorage::READ);
	if(!fs.isOpened()){
		fprintf(stderr, "ERROR: Failed to open calibration file: %s\n", intrinsics_file);
		return -1;
	}
	n = fs["width"];
	if(n.type() == FileNode::NONE){
		fprintf(stderr, "ERROR failed to find width in %s\n", intrinsics_file);
		return -1;
	}
	else n >> w;

	n = fs["height"];
	if(n.type() == FileNode::NONE){
		fprintf(stderr, "ERROR failed to find height in %s\n", intrinsics_file);
		return -1;
	}
	else n >> h;

	if(w == -1 || h == -1)
	{
		fprintf(stderr, "ERROR: Failed to read width and height from %s\n", intrinsics_file);
		return -1;
	}

	width[ch] = w;
	height[ch] = h;
	return 0;
}


static int _load_calibration(void)
{
	printf("loading calibration files\n");
	for(int i=0; i<MAX_PAIRS; i++){

		if(!conf[i].enable) continue;

		// populate static width/height arrays
		_get_w_h_from_intrinsics_file(i, conf[i].intrinsics_file);

		if(conf[i].en_cvp_rectification)
		{
			// compute undistort grids from stereo params
			undistort_grid_l[i] = mcv_cvp_create_undistort_grid(width[i], height[i]);
			undistort_grid_r[i] = mcv_cvp_create_undistort_grid(width[i], height[i]);
			if(mcv_create_stereo_undistort_grid(conf[i].intrinsics_file, 
				conf[i].extrinsics_file, &cvp_stereo_params[i], undistort_grid_l[i],
				undistort_grid_r[i]))
			{
				fprintf(stderr, "ERROR computing stereo undistort grids\n");
				return -1;
			}
			
			// pass baseline information in
			baseline_m[i] = cvp_stereo_params[i].baseline_m;

			// validate configuration
			if(width[i]!=cvp_stereo_params[i].w || height[i]!=cvp_stereo_params[i].h){
				fprintf(stderr, "ERROR loading calibration files, image dimensions do not match\n");
				return -1;
			}
			printf("for Pair #%d, loaded these lens params:\n", i);
			printf("w:  %d\n", width[i]);
			printf("h:  %d\n", height[i]);
			printf("f:  %f\n", (double)cvp_stereo_params[i].fxrect);
			printf("cx: %f\n", (double)cvp_stereo_params[i].cxrect);
			printf("cy: %f\n", (double)cvp_stereo_params[i].cyrect);
			printf("baseline_m: %f\n", (double)cvp_stereo_params[i].baseline_m);

		} else {
			int ret = mcv_init_stereo_undistort_maps(conf[i].intrinsics_file, \
													 conf[i].extrinsics_file, \
													 &mapL[i], &mapR[i], \
													 &baseline_m[i]);
			if(ret) return -1;
			if(width[i]!=mapL[i].w || height[i]!=mapL[i].h){
				fprintf(stderr, "ERROR loading calibration files, image dimensions do not match\n");
				return -1;
			}

			printf("for Pair #%d, loaded these lens params:\n", i);
			printf("w:  %d\n", width[i]);
			printf("h:  %d\n", height[i]);
			printf("f:  %f\n", (double)mapL[i].fxrect);
			printf("cx: %f\n", (double)mapL[i].cxrect);
			printf("cy: %f\n", (double)mapL[i].cyrect);
			printf("baseline_m: %f\n", (double)baseline_m[i]);
		}

	}
	return 0;
}

static int _setup_dfs(void)
{
	// Set OpenCV to use only 1 thread! multithreading doesn't make anything
	// faster but can definitely waste more gpu and memory bandwidth.
	//cv::setNumThreads(0);

	// set up the dfs module

	// iterate over each thread, configuring individually
	for(int i=0; i<MAX_PAIRS; i++){

		if(!conf[i].enable) continue;
		if(en_debug) printf("initializing dfs module\n");
		mcv_dfs_config_t dfs_config;
		dfs_config.width                     = width[i];
		dfs_config.height                    = height[i];
		dfs_config.n_disparity_levels        = conf[i].max_disparity;
		dfs_config.starting_disparity_offset = conf[i].min_disparity;   // Valid range is [0, 254-nMaxDisparity]
		dfs_config.en_median_filter          = 1;                       // enable median filtering
		dfs_config.en_occlusion_filling      = 0;                       // don't fill in data to occluded areas
		dfs_config.occlusion_min_threshold   = conf[i].min_threshold;   // [0-100] nOcclusionMinThreshold <= nOcclusionThreshold
		dfs_config.occlusion_cost_threshold  = conf[i].cost_threshold;  // [0-100] nOcclusionMinThreshold <= nOcclusionThreshold

		// allocate memory for future steps
		int size = width[i]*height[i];
		imgL_rect[i]		= (uint8_t*)malloc(size);
		imgR_rect[i]		= (uint8_t*)malloc(size);
		disparity_raster[i]	= (uint8_t*)malloc(size);
		disparity_lin[i]	= (uint8_t*)malloc(size);
		disparity_scaled[i]	= (uint8_t*)malloc(size);
		occlusion_mask[i]   = (uint8_t*)malloc(size);
		occlusion_lin[i]    = (uint8_t*)malloc(size);

		// undistort configurations for step i
		mcv_undistort_config_t undistort_config_l, undistort_config_r;
		undistort_config_l.width           = width[i];
		undistort_config_l.height          = height[i];
		undistort_config_l.warp_mode       = MCV_WARP_GRID;
		undistort_config_l.border_mode     = MCV_BORDER_CONSTANT;
		undistort_config_l.image_mode      = IMAGE_FORMAT_RAW8;
		undistort_config_r = undistort_config_l;

		// pass in grids for undistortion
		undistort_config_l.undistort_grid = undistort_grid_l[i];
		undistort_config_r.undistort_grid = undistort_grid_r[i];

		// pointcloud population
		mcv_pc_disp_config_t pc_config;
		pc_config.width         = width[i];
		pc_config.height        = height[i];
		pc_config.baseline_m    = baseline_m[i];
		pc_config.skip_lines    = conf[i].pc_skip_n_lines;  // only do some of the points
		pc_config.min_disparity = conf[i].min_disparity;

		// populate corresponding handle, configuring Undistort+DFS if CVP
		// is chosen and only DFS otherwise. set other handle to NULL
		if(conf[i].en_cvp_rectification)
		{
			printf("ch%d=CVP\n", i);
			mcv_cvp_undistort_and_dfs_handle handle;
			handle = mcv_cvp_undistort_and_dfs_init(undistort_config_l, 
				undistort_config_r, dfs_config);
			if(NULL == handle)
			{
				fprintf(stderr, "ERROR: NULL Undistort+DFS handle\n");
			}
			ud_dfs_h[i] = handle;
			dfs_h[i] = NULL;

			// CVP PC population
			pc_config.f  = cvp_stereo_params[i].fxrect;
			pc_config.cx = roundf(cvp_stereo_params[i].cxrect);
			pc_config.cy = roundf(cvp_stereo_params[i].cyrect);
		} else {
			printf("ch%d=CPU\n", i);
			mcv_cvp_dfs_handle handle;
			handle = mcv_cvp_dfs_init(dfs_config);
			if(NULL == handle)
			{
				fprintf(stderr, "ERROR: NULL DFS handle\n");
			}
			dfs_h[i] = handle;
			ud_dfs_h[i] = NULL;

			// CPU PC population
			pc_config.f  = mapL[i].fxrect;
			pc_config.cx = roundf(mapL[i].cxrect);
			pc_config.cy = roundf(mapL[i].cyrect);
		}

		if(mcv_pc_disp_table_init(pc_config, &pc_disp_table[i])) return -1;
	}
	printf("Setup finished\n");

	return 0;
}

/**
 * @brief Compute undistortion on the CPU and then DFS on CVP
 * 
 * Note: This function is a slight misnomer as only the undistort is computed
 * on the CPU, the DFS is computed on CVP
 * 
 * @param ch Channel
 * @param imgL_filt Left image
 * @param imgR_filt Right image
 * @return 0 if successful, -1 otherwise 
 */
static int _undistort_and_dfs_cpu(int ch, Mat imgL_filt, Mat imgR_filt)
{
	pthread_t thread1, thread2;
	mcv_undistort_image_async(imgL_filt.data, imgL_rect[ch], &mapL[ch], &thread1);
	mcv_undistort_image_async(imgR_filt.data, imgR_rect[ch], &mapR[ch], &thread2);
	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);

	// do the dfs processing and linearize
	is_using_cvp[ch]=1;
	if(mcv_cvp_dfs_process(dfs_h[ch], imgL_rect[ch], imgR_rect[ch], disparity_raster[ch], NULL)){
		fprintf(stderr, "ERROR performing DFS process on CVP\n");
		return -1;
	}
	is_using_cvp[ch]=0;
	return 0;
}

/**
 * @brief Use the Undistortion+DFS pipeline on CVP hardware 
 * 
 * @param ch Channel
 * @param imgL_filt Left image
 * @param imgR_filt Right image
 * @return 0 if successful, -1 otherwise 
 */
static int _undistort_and_dfs_cvp(int ch, Mat imgL_filt, Mat imgR_filt)
{
	is_using_cvp[ch] = 1;
	if(mcv_cvp_undistort_and_dfs_process(ud_dfs_h[ch], imgL_filt.data, 
		imgR_filt.data, disparity_raster[ch], NULL))
	{
		fprintf(stderr, "ERROR performing undistort+DFS pipeline\n");
		return -1;
	}
	is_using_cvp[ch] = 0;
	return 0;
}

static void _image_callback(int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context)
{
	camera_image_metadata_t disp_meta;
	int i;
	int64_t t[7] = {0}; // for timing mode


	// Check to make sure the format of the frame is supported
	if(meta.format != IMAGE_FORMAT_STEREO_RAW8){
		fprintf(stderr, "ERROR only support STEREO_RAW_8 images right now\n");
		return;
	}
	if(meta.width!=width[ch] || meta.height!=height[ch]){
		fprintf(stderr, "ERROR wrong image resolution, expected %dx%d\n", width[ch], height[ch]);
		return;
	}

	// skip frame if there are no clients and not in debug mode
	if(!_has_clients(ch) && !en_debug && !en_timing){
		return;
	}

	// skip a frame if getting backed up
	if(pipe_client_bytes_in_pipe(ch)>0){
		n_skipped[ch]++;
		if(en_debug){
			fprintf(stderr, "ch%d skipping frame due to frame backup\n", ch);
		}
		return;
	}

	// skip frames if requested
	if(n_skipped[ch] < conf[ch].skip_n_frames){
		if(en_debug) printf("ch%d skipping required frame\n", ch);
		n_skipped[ch]++;
		return;
	}


	// start processing the frame, start skip counter back at 0
	n_skipped[ch] = 0;
	if(en_debug){
		fprintf(stderr, "ch%d starting processing\n", ch);
	}


	////////////////////////////////////////////////////////////////////////////
	// Start processing
	////////////////////////////////////////////////////////////////////////////
	if(en_timing) t[0] = _apps_time_monotonic_ns();

	// convert to opencv matrix just to do the blur for now until we write our own
	Mat imgL_raw(height[ch], width[ch], CV_8UC1, frame);
	Mat imgR_raw(height[ch], width[ch], CV_8UC1, frame + (width[ch]*height[ch]));

	// do gaussian blur
	Mat imgL_filt, imgR_filt;
	if(conf[ch].blur_size >= 3){
		Size blursize = Size(conf[ch].blur_size,conf[ch].blur_size);
		GaussianBlur(imgL_raw, imgL_filt, blursize, 0, 0, BORDER_DEFAULT);
		GaussianBlur(imgR_raw, imgR_filt, blursize, 0, 0, BORDER_DEFAULT);
	}
	// skip if blur is disabled
	else{
		imgL_filt = imgL_raw;
		imgR_filt = imgR_raw;
	}

	// when not running flat out, try to keep images from overlapping CVP use
	if(conf[ch].skip_n_frames>0 && ch==1){
		if(is_using_cvp[0]){
			if(en_debug){
				printf("ch%d skipping to get in sync\n", ch);
			}
			n_skipped[ch]++;
			return;
		}
	}

	// either pipeline CVP Undistort+DFS or use CPU Undistort and CVP DFS
	if(en_timing) t[1] = _apps_time_monotonic_ns();
	if(conf[ch].en_cvp_rectification)
	{
		if(_undistort_and_dfs_cvp(ch, imgL_filt, imgR_filt))
		{
			fprintf(stderr, "ERROR computing UD+DFS pipeline on CVP\n");
			return; 
		}
	} else {
		if(_undistort_and_dfs_cpu(ch, imgL_filt, imgR_filt))
		{
			fprintf(stderr, "ERROR computing CPU UD or CVP DFS\n");
			return;
		}
	}
	if(en_timing) t[2] = _apps_time_monotonic_ns();

	if(mcv_dfs_linearize_disparity(disparity_raster[ch], width[ch], height[ch], disparity_lin[ch])){
		return;
	}
	if(mcv_dfs_linearize_occlusion(occlusion_mask[ch], width[ch], height[ch], occlusion_lin[ch])){
		return;
	}
	if(en_timing) t[3] = _apps_time_monotonic_ns();


	// post-process with another median filter
	if(conf[ch].post_median_size>=3){
		Mat disparity_filtered_ocv;
		Mat disparity_lin_ocv(height[ch], width[ch], CV_8UC1, disparity_lin[ch]);
		cv::medianBlur(disparity_lin_ocv, disparity_filtered_ocv, conf[ch].post_median_size);
		memcpy(disparity_lin[ch], disparity_filtered_ocv.data, width[ch]*height[ch]);
	}
	if(en_timing) t[4] = _apps_time_monotonic_ns();


	////////////////////////////////////////////////////////////////////////////
	// send disparity image
	////////////////////////////////////////////////////////////////////////////
	int disparity_ch = (ch*OUT_PIPES_PER_PAIR)+PIPE_OFFSET_DISPARITY;
	if(pipe_server_get_num_clients(disparity_ch)>0){
		disp_meta = meta;
		disp_meta.format = IMAGE_FORMAT_RAW8;
		disp_meta.size_bytes = width[ch]*height[ch];
		pipe_server_write_camera_frame(disparity_ch, disp_meta, (char*)disparity_lin[ch]);
	}

	////////////////////////////////////////////////////////////////////////////
	// make scaled debug image and send
	////////////////////////////////////////////////////////////////////////////
	int scaled_ch = (ch*OUT_PIPES_PER_PAIR)+PIPE_OFFSET_DISPARITY_SCALED;
	if(pipe_server_get_num_clients(scaled_ch)>0 || en_timing){
		// since we will only see conf->max_disparity as the max value, we can 
		// linearly scale this up to 255 by multiplying by (int)256/conf->max_disparity
		uint64_t* in  = (uint64_t*)disparity_lin[ch];
		uint64_t* out = (uint64_t*)disparity_scaled[ch];
		int multiplier = 256 / (conf[ch].max_disparity + conf[ch].min_disparity);    // smallest integer multiplier that can fit into 256
		for(i=0; i<(width[ch]*height[ch]/8); i++){
			out[i]=in[i] * multiplier;
		}
		disp_meta = meta;
		disp_meta.format = IMAGE_FORMAT_RAW8;
		disp_meta.size_bytes = width[ch]*height[ch];
		pipe_server_write_camera_frame(scaled_ch, disp_meta, (char*)disparity_scaled[ch]);
	}

	if(en_timing) t[5] = _apps_time_monotonic_ns();


	////////////////////////////////////////////////////////////////////////////
	// make point cloud
	////////////////////////////////////////////////////////////////////////////
	int pc_ch = (ch*OUT_PIPES_PER_PAIR)+PIPE_OFFSET_POINT_CLOUD;
	if(pipe_server_get_num_clients(pc_ch)>0 || en_timing){

		mcv_pc_t pc = MCV_PC_INITIALIZER;
		mcv_pc_from_disparity(disparity_lin[ch], &pc_disp_table[ch], &pc);

		// if there were no points from the disparity image, don't bother sending
		if(pc.n > 0 && pc.initialized){
			point_cloud_metadata_t pc_meta;
			pc_meta.magic_number = POINT_CLOUD_MAGIC_NUMBER;
			pc_meta.timestamp_ns = meta.timestamp_ns + (meta.exposure_ns/2);
			pc_meta.n_points = pc.n;
			pc_meta.format = POINT_CLOUD_FORMAT_FLOAT_XYZ;
			pipe_server_write_point_cloud(pc_ch, pc_meta, pc.d);
		}

		mcv_pc_free(&pc);
	}
	if(en_timing) t[6] = _apps_time_monotonic_ns();

	if(en_timing){
		printf("ch %d times:\n", ch);
		printf("%6.2fms   pre: blur\n", ((double)(t[1]-t[0]))/1000000.0);
		printf("%6.2fms   undistort+DFS\n", ((double)(t[2]-t[1]))/1000000.0);
		printf("%6.2fms   disparity\n", ((double)(t[3]-t[2]))/1000000.0);
		printf("%6.2fms   post: median filter\n",  ((double)(t[4]-t[3]))/1000000.0);
		printf("%6.2fms   scale & send image\n",   ((double)(t[5]-t[4]))/1000000.0);
		printf("%6.2fms   generate point cloud\n", ((double)(t[6]-t[5]))/1000000.0);
		printf("%6.2fms   total time\n\n", ((double)(t[6]-t[0]))/1000000.0);
	}

	//if(en_debug || en_timing) printf("\n");
	double total_time = (double)((t[6]-t[0])/1000000.0);
	if(en_timing)
	{
		if(callback_times.size() > max_vec_size)
		{
			callback_times.erase(callback_times.begin());
		}
		callback_times.push_back(total_time);
	}

	return;
}


// TODO put this in libmodal_pipe
static int _pipe_trim_name_from_path(const char* path, char* name, int max_namelen)
{

	int len = strlen(path);
	// TODO test for more edge cases
	if(len<1){
		fprintf(stderr, "ERROR in %s, recevied empty string\n", __FUNCTION__);
		return -1;
	}
	if(len==1 && path[0]=='/'){
		fprintf(stderr, "ERROR in %s, pipe path can't just be root '/'\n", __FUNCTION__);
		return -1;
	}

	int end = 0;
	int start = 0;

	// work from the back finding start and end of name string
	for(int i=len; i>=0; i--){
		// searching for end
		if(end==0){
			if(path[i]!='/'){
				end = i;
				continue;
			}
		}
		else{
			if(path[i]=='/'){
				start = i;
				break;
			}
		}
	}

	int namelen = end-start;
	if(end==0 || namelen<1){
		fprintf(stderr, "ERROR in %s, failed to parse string\n", __FUNCTION__);
		return -1;
	}
	// check length, accounting for the null terminator
	if(namelen>=max_namelen){
		fprintf(stderr, "ERROR in %s, name too long\n", __FUNCTION__);
		return -1;
	}

	memcpy(name, &path[start], namelen);
	name[namelen]=0; // flag end of the string

	return 0;
}


static int _setup_server_pipes(void)
{
	int flags = 0;

	for(int i=0; i<MAX_PAIRS; i++){

		if(!conf[i].enable) continue;

		// extract name from config file, e.g. stereo_front
		char name[MODAL_PIPE_MAX_NAME_LEN];
		if(_pipe_trim_name_from_path(conf[i].input_pipe, name, MODAL_PIPE_MAX_NAME_LEN)){
			return -1;
		}

		// disparity pipe
		int disp_ch = (i*OUT_PIPES_PER_PAIR)+PIPE_OFFSET_DISPARITY;
		pipe_info_t info1 = { \
			{0},	// name
			{0},	// location
			"camera_image_metadata_t",	// type
			PROCESS_NAME,				// server_name
			(8*1024*1024),				// size_bytes
			0							// server_pid
		};
		strcpy(info1.name, name);
		strcat(info1.name, "_disparity");
		//printf("newname: %s\n", info1.name);
		pipe_server_set_connect_cb(disp_ch, _client_connect_handler, NULL);
		pipe_server_set_disconnect_cb(disp_ch, _client_disconnect_handler, NULL);
		if(pipe_server_create(disp_ch, info1, flags)) return -1;


		// scaled pipe
		int scaled_ch = (i*OUT_PIPES_PER_PAIR)+PIPE_OFFSET_DISPARITY_SCALED;
		pipe_info_t info2 = { \
			{0},	// name
			{0},	// location
			"camera_image_metadata_t",		// type
			PROCESS_NAME,					// server_name
			(8*1024*1024),					// size_bytes
			0								// server_pid
		};
		strcpy(info2.name, name);
		strcat(info2.name, "_disparity_scaled");
		pipe_server_set_connect_cb(scaled_ch, _client_connect_handler, NULL);
		pipe_server_set_disconnect_cb(scaled_ch, _client_disconnect_handler, NULL);
		if(pipe_server_create(scaled_ch, info2, flags)) return -1;

		// point cloud pipe
		int pc_ch = (i*OUT_PIPES_PER_PAIR)+PIPE_OFFSET_POINT_CLOUD;
		pipe_info_t info3 = { \
			{0},	// name
			{0},	// location
			"point_cloud_metadata_t",		// type
			PROCESS_NAME,					// server_name
			(64*1024*1024),					// size_bytes
			0								// server_pid
		};
		strcpy(info3.name, name);
		strcat(info3.name, "_pc");
		pipe_server_set_connect_cb(pc_ch, _client_connect_handler, NULL);
		pipe_server_set_disconnect_cb(pc_ch, _client_disconnect_handler, NULL);
		if(pipe_server_create(pc_ch, info3, flags)) return -1;
	}

	return 0;
}



static int _setup_client_pipes(void)
{
	int flags = CLIENT_FLAG_EN_CAMERA_HELPER;

	if(!en_debug && !en_timing){
		printf("opening cameras paused\n");
		flags |= CLIENT_FLAG_START_PAUSED;
	}

	for(int i=0; i<MAX_PAIRS; i++){

		if(!conf[i].enable) continue;

		pipe_client_set_camera_helper_cb(i, _image_callback, NULL);
		pipe_client_set_connect_cb(i, _camera_connect_cb, NULL);
		pipe_client_set_disconnect_cb(i, _camera_disconnect_cb, NULL);

		int ret = pipe_client_open(i, conf[i].input_pipe, PROCESS_NAME, flags, 0);
		if(ret < 0){
			fprintf(stderr, "ERROR: critical failure subscribing to pipe\n");
			pipe_print_error(ret);
			return -1;
		}
	}

	return 0;
}


int main(int argc, char *argv[])
{
	// Parse the command line options and terminate if the parser says we should terminate
	if(_parse_opts(argc, argv)){
		return -1;
	}

	// load and print config file
	if(config_file_read()){
		return -1;
	}
	config_file_print();

	/* make sure another instance isn't running
	* if return value is -3 then a background process is running with
	* higher privileges and we couldn't kill it, in which case we should
	* not continue or there may be hardware conflicts. If it returned -4
	* then there was an invalid argument that needs to be fixed.
	*/
	if(kill_existing_process(PROCESS_NAME, 2.0) < -2){
		// Exit the app with an error code
		exit(-1);
	}

	// start signal handler so we can exit cleanly
	if(enable_signal_handler() == -1){
		fprintf(stderr, "ERROR: failed to start signal handler\n");
		exit(-1);
	}

	/* make PID file to indicate your project is running
	 * due to the check made on the call to rc_kill_existing_process() above
	 * we can be fairly confident there is no PID file already and we can
	 * make our own safely.
	 */
	make_pid_file(PROCESS_NAME);

	// load in calibration files
	if(_load_calibration()) _quit(-1);

	// configure DFS processing stuff
	if(_setup_dfs()) _quit(-1);

	// set up pipes, client-interface starts paused until we get our own subscriber
	if(_setup_client_pipes()) _quit(-1);
	if(_setup_server_pipes()) _quit(-1);

	// wait for signal to close
	main_running = 1;
	while(main_running){
		usleep(5000000);
	}

	// exit cleanly
	_quit(0);
	return 0;
}

