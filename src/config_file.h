/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @headerfile  config_file.h
 *
 * @brief       Configures the DFS Server in main.cpp
 *
 * @author      James Strawson
 * @author      Thomas Patton (thomas.patton@modalai.com) 
 * @date        2023 
 */

#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#ifdef __cplusplus
extern "C" {
#endif


#include <modal_json.h>

#define CONF_FILE "/etc/modalai/voxl-dfs-server.conf"
#define MAX_PAIRS	 2 


#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration parameters for voxl-dfs-server.\n\
 * You can specify up to 2 pairs to do detection on simultaneously.\n\
 *\n\
 *\n\
 * min_disparity:     minimum disparity that is checked, default 4 since objects\n\
 *                    really far away are hard to detect reliably anyway.\n\
 *\n\
 * max_disparity:     default 64, can decrease to improve latency slightly\n\
 *                    maximum value is 64, 48 is a good option\n\
 *\n\
 * min_threshold:     must be <= cost_threshold, typically the same as cost_threshold,\n\
 *                    must be in the range [0,100], default 10\n\
 *\n\
 * cost_threshold:    must be in the range [0,100], default 10. Lower numbers will\n\
 *                    be more conservative and only detect matches with higher confidence\n\
 *\n\
 * width/ height:     All input images must have this resolution\n\
 *\n\
 * pc_skip_n_lines:   lines/columns to skip when outputting a point cloud. Set to\n\
 *                    0 to output one point for every pixel, althought this is not\n\
 *                    recommended since the resoluting point cloud would be huge.\n\
 *                    default is 4, meaning generate point for every 5th row/column\n\
 *\n\
 * blur_size:         Optional gaussian blur before stereo match for particularly\n\
 *                    noisy images. Off by default. Must be an odd number or set \n\
 *                    to 0 to disable.\n\
 *\n\
 * skip_n_frames:     Automatically skip this number of input frames. Default 1 meaning\n\
 *                    every other stereo frame is processed. Frames will be Automatically\n\
 *                    skipped if the cpu can't keep up.\n\
 *                    \n\
 * post_median_size:  optional median filter after disparity matching. Can help to\n\
 *                    reduce speckles but that's usually best done in 3D not 2D.\n\
 *                    Off by default. Must be an odd number if you turn it on, a good\n\
 *                    starting point is 15. This requires additional CPU.\n\
 * en_cvp_rectification: If true, performs image undistortion using CVP \n\
 *                       hardware. \n\
 *\n\
 */\n"


typedef struct dfs_config_t{
	int enable;
	char input_pipe[MODAL_PIPE_MAX_DIR_LEN];
	int skip_n_frames;
	int blur_size;
	int post_median_size;
	char intrinsics_file[MODAL_PIPE_MAX_PATH_LEN];
	char extrinsics_file[MODAL_PIPE_MAX_PATH_LEN];
	int en_cvp_rectification;
	int min_disparity;
	int max_disparity;
	int min_threshold;
	int cost_threshold;
	int pc_skip_n_lines;
} dfs_config_t;

static dfs_config_t conf[MAX_PAIRS];

/**
 * @brief      prints the current configuration values to the screen
 *
 *             this includes all of the extern variables listed above. If this
 *             is called before config_file_load then it will print the default
 *             values.
 */
static void config_file_print(void)
{
	printf("=================================================================\n");
	for(int i=0; i<MAX_PAIRS; i++){
		printf("dfs pair #%d\n", i);
		printf("    enable:               %d\n", conf[i].enable);
		printf("    input_pipe:           %s\n", conf[i].input_pipe);
		printf("    skip_n_frames:        %d\n", conf[i].skip_n_frames);
		printf("    blur_size:            %d\n", conf[i].blur_size);
		printf("    post_median_size:     %d\n", conf[i].post_median_size);
		printf("    intrinsics_file:      %s\n", conf[i].intrinsics_file);
		printf("    extrinsics_file:      %s\n", conf[i].extrinsics_file);
		printf("    en_cvp_rectification: %d\n", conf[i].en_cvp_rectification);
		printf("    min_disparity:        %d\n", conf[i].min_disparity);
		printf("    max_disparity:        %d\n", conf[i].max_disparity);
		printf("    min_threshold:        %d\n", conf[i].min_threshold);
		printf("    cost_threshold:       %d\n", conf[i].cost_threshold);
		printf("    pc_skip_n_lines:      %d\n", conf[i].pc_skip_n_lines);
	}
	printf("=================================================================\n");
	return;
}


/**
 * load the config file and populate the above extern variables
 *
 * @return     0 on success, -1 on failure
 */
static int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", CONF_FILE);

	cJSON* parent = json_read_file(CONF_FILE);
	if(parent==NULL) return -1;

	// if old version of a config file, overwrite!
	if(NULL == cJSON_GetObjectItem(parent, "config_file_version"))
	{
		parent = cJSON_CreateObject();
	}

	int ver;
	json_fetch_int_with_default(    parent, "config_file_version",              &ver,                           1);

	json_fetch_int_with_default(	parent, "dfs_pair_0_enable",				&conf[0].enable,				1);
	json_fetch_string_with_default(	parent, "dfs_pair_0_input_pipe",			conf[0].input_pipe,				MODAL_PIPE_MAX_DIR_LEN, "stereo_front");
	json_fetch_int_with_default(	parent, "dfs_pair_0_skip_n_frames",			&conf[0].skip_n_frames,			1);
	json_fetch_int_with_default(	parent, "dfs_pair_0_blur_size",				&conf[0].blur_size,				0);
	json_fetch_int_with_default(	parent, "dfs_pair_0_post_median_size",		&conf[0].post_median_size,		0);
	json_fetch_string_with_default(	parent, "dfs_pair_0_intrinsics_file",		conf[0].intrinsics_file,		MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_stereo_front_intrinsics.yml");
	json_fetch_string_with_default(	parent, "dfs_pair_0_extrinsics_file",		conf[0].extrinsics_file,		MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_stereo_front_extrinsics.yml");
	json_fetch_int_with_default(	parent, "dfs_pair_0_en_cvp_rectification",  &conf[0].en_cvp_rectification,	1);
	json_fetch_int_with_default(    parent, "dfs_pair_0_min_disparity",         &conf[0].min_disparity,         4);
	json_fetch_int_with_default(    parent, "dfs_pair_0_max_disparity",         &conf[0].max_disparity,         64);
	json_fetch_int_with_default(    parent, "dfs_pair_0_min_threshold",         &conf[0].min_threshold,         10);
	json_fetch_int_with_default(    parent, "dfs_pair_0_cost_threshold",        &conf[0].cost_threshold,        10);
	json_fetch_int_with_default(    parent, "dfs_pair_0_pc_skip_n_lines",       &conf[0].pc_skip_n_lines,       4);

	json_fetch_int_with_default(	parent, "dfs_pair_1_enable",				&conf[1].enable,				1);
	json_fetch_string_with_default(	parent, "dfs_pair_1_input_pipe",			conf[1].input_pipe,				MODAL_PIPE_MAX_DIR_LEN, "stereo_rear");
	json_fetch_int_with_default(	parent, "dfs_pair_1_skip_n_frames",			&conf[1].skip_n_frames,			1);
	json_fetch_int_with_default(	parent, "dfs_pair_1_blur_size",				&conf[1].blur_size,				0);
	json_fetch_int_with_default(	parent, "dfs_pair_1_post_median_size",		&conf[1].post_median_size,		0);
	json_fetch_string_with_default(	parent, "dfs_pair_1_intrinsics_file",		conf[1].intrinsics_file,		MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_stereo_rear_intrinsics.yml");
	json_fetch_string_with_default(	parent, "dfs_pair_1_extrinsics_file",		conf[1].extrinsics_file,		MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_stereo_rear_extrinsics.yml");
	json_fetch_int_with_default(	parent, "dfs_pair_1_en_cvp_rectification",  &conf[1].en_cvp_rectification,	1);
	json_fetch_int_with_default(    parent, "dfs_pair_1_min_disparity",         &conf[1].min_disparity,         4);
	json_fetch_int_with_default(    parent, "dfs_pair_1_max_disparity",         &conf[1].max_disparity,         64);
	json_fetch_int_with_default(    parent, "dfs_pair_1_min_threshold",         &conf[1].min_threshold,         10);
	json_fetch_int_with_default(    parent, "dfs_pair_1_cost_threshold",        &conf[1].cost_threshold,        10);
	json_fetch_int_with_default(    parent, "dfs_pair_1_pc_skip_n_lines",       &conf[1].pc_skip_n_lines,       4);

	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		json_write_to_file_with_header(CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);

	// make sure at least one detector is enabled
	int n_enabled = 0;
	for(int i=0; i<MAX_PAIRS; i++){
		if(conf[i].enable){
			n_enabled++;
		}
	}
	if(n_enabled<=0){
		fprintf(stderr, "ERROR at least one detector must be enabled\n");
		return -1;
	}

	// check blur for validity
	for(int i=0; i<MAX_PAIRS; i++){
		if(conf[i].enable){
			int s = conf[i].blur_size;
			if(!(s==0 || (((s%2)==1) && s>=3))){
				fprintf(stderr, "ERROR, blur size must be 0 or an odd number >=3\n");
				return -1;
			}
		}
	}

	// check median for validity
	for(int i=0; i<MAX_PAIRS; i++){
		if(conf[i].enable){
			int s = conf[i].post_median_size;
			if(!(s==0 || (((s%2)==1) && s>=3))){
				fprintf(stderr, "ERROR, post median size must be 0 or an odd number >=3\n");
				return -1;
			}
		}
	}
	// check cost thresholds for validity
	for(int i=0; i<MAX_PAIRS; i++){
		if(conf[i].enable){
			if(conf[i].min_threshold>100 || conf[i].min_threshold<0){
				fprintf(stderr, "ERROR, min_threshold must be in [0,100]\n");
				return -1;
			}
		}
	}
	for(int i=0; i<MAX_PAIRS; i++){
		if(conf[i].enable){
			if(conf[i].cost_threshold>100 || conf[i].cost_threshold<0){
				fprintf(stderr, "ERROR, cost_threshold must be in [0,100]\n");
				return -1;
			}
		}
	}
	for(int i=0; i<MAX_PAIRS; i++){
		if(conf[i].enable){
			if(conf[i].min_threshold>conf[i].cost_threshold){
				fprintf(stderr, "ERROR, min_threshold must be <= cost_threshold\n");
				return -1;
			}
		}
	}
	for(int i=0; i<MAX_PAIRS; i++){
		if(conf[i].enable){
			if(conf[i].max_disparity>64){
				fprintf(stderr, "ERROR, max_disparity must be <=64\n");
				return -1;
			}
		}
	}
	return 0;
}


#ifdef __cplusplus
}
#endif


#endif // end CONFIG_FILE_H
